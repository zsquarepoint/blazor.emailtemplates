﻿using eComEngine.Domain.Entities;

namespace eComEngine.Application.Models.Dto
{
    public class DataResponseDto
    {
        public IEnumerable<EmailTemplate>? EmailTemplates { get; set; }

        public int TotalTemplateCount { get; set; } = 0;

        public int TotalPages { get; set; } = 0;
    }
}