﻿namespace eComEngine.Application.Models.Dto
{
    public class DataFilterDto
    {
        public PaginationDto Pagination { get; set; }

        public string SortBy { get; set; } = "el-asc";

        public string SearchBy { get; set; } = "";
    }
}