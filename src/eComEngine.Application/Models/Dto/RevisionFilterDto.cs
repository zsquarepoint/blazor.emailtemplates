﻿namespace eComEngine.Application.Models.Dto
{
    public class RevisionFilterDto
    {
        public string Id { get; set; }
    }
}