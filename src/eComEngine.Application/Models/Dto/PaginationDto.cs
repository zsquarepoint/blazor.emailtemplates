﻿namespace eComEngine.Application.Models.Dto
{
    public class PaginationDto
    {
        public int Page { get; set; } = 1;

        public int QuantityPerPage { get; set; } = 10;

        public string SortBy { get; set; } = "el-asc";
    }
}