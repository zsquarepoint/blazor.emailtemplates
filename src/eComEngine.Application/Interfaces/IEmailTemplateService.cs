﻿using eComEngine.Application.Models.Dto;

namespace eComEngine.Application.Interfaces
{
    public interface IEmailTemplateService
    {
        Task<DataResponseDto> GetTemplatesAsync(string targetUri, DataFilterDto filters);

        Task<DataResponseDto> GetRevisionsAsync(string targetUri, RevisionFilterDto filters);
    }
}