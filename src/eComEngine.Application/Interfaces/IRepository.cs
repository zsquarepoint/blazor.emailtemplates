﻿using eComEngine.Domain.Entities;
using System.Linq.Expressions;

namespace eComEngine.Application.Interfaces
{
    public interface IRepository<T>
    {
        Task<T> Single(Guid id);

        Task<IQueryable<T>> Where(Expression<Func<T, bool>> selector);

        Task<IQueryable<EmailTemplate>> Where(Expression<Func<EmailTemplate, bool>> selector,
                                              Expression<Func<IQueryable<EmailTemplate>, IQueryable<EmailTemplate>>> orderBy);
    }
}