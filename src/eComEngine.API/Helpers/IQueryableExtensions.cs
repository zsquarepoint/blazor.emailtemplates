﻿using eComEngine.Application.Models.Dto;

namespace eComEngine.API.Helpers
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> Paginate<T>(this IQueryable<T> queryable, PaginationDto pagination)
        {
            return queryable
                    .Skip((pagination.Page - 1) * pagination.QuantityPerPage)
                    .Take(pagination.QuantityPerPage);
        }
    }
}