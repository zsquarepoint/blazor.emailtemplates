﻿using eComEngine.API.Helpers;
using eComEngine.API.Services;
using eComEngine.Application.Models.Dto;
using eComEngine.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace eComEngine.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailTemplatesController : ControllerBase
    {
        public EmailTemplatesController()
        {
        }

        [HttpPost]
        [Route(nameof(GetPaginatedList))]
        public async Task<ActionResult<DataResponseDto>> GetPaginatedList([FromBody] DataFilterDto filters)
        {
            //JIC: safety precaution
            filters.Pagination.Page = filters.Pagination.Page <= 0 ? 1 : filters.Pagination.Page;
            filters.Pagination.QuantityPerPage = filters.Pagination.QuantityPerPage <= 0 ? 10 : filters.Pagination.QuantityPerPage;

            //init template repository
            EmailTemplateXmlFileRepository templateFileRepo = new EmailTemplateXmlFileRepository();

            //get/set queryable templates => filtered by Active & IsDefault
            IQueryable<EmailTemplate>? queryableTemplates = templateFileRepo.Where(t => t.Active && t.IsDefault).Result;

            double totalTemplateCount = 0.0;
            var totalPages = 0;

            IQueryable<EmailTemplate>? templates = null;

            if (queryableTemplates != null)
            {
                //search by
                if (!string.IsNullOrWhiteSpace(filters.SearchBy))
                {
                    queryableTemplates = queryableTemplates.Where(t => t.EmailLabel.Contains(filters.SearchBy) || t.FromAddress.Contains(filters.SearchBy));
                }

                //sort by
                switch (filters.SortBy)
                {
                    case "el-asc": queryableTemplates = queryableTemplates.OrderBy(t => t.EmailLabel); break;
                    case "el-desc": queryableTemplates = queryableTemplates.OrderByDescending(t => t.EmailLabel); break;
                    case "fa-asc": queryableTemplates = queryableTemplates.OrderBy(t => t.FromAddress); break;
                    case "fa-desc": queryableTemplates = queryableTemplates.OrderByDescending(t => t.FromAddress); break;
                    case "du-asc": queryableTemplates = queryableTemplates.OrderBy(t => t.DateUpdated); break;
                    case "du-desc": queryableTemplates = queryableTemplates.OrderByDescending(t => t.DateUpdated); break;
                }

                totalTemplateCount = queryableTemplates.Count();
                totalPages = (int)Math.Ceiling(totalTemplateCount / filters.Pagination.QuantityPerPage);

                templates = queryableTemplates.Paginate(filters.Pagination);
            }

            //return data response
            var dataResponse = new DataResponseDto
            {
                TotalTemplateCount = (int)totalTemplateCount,
                TotalPages = totalPages,
                EmailTemplates = templates
            };

            return Ok(dataResponse);
        }

        [HttpPost]
        [Route(nameof(GetRevisionList))]
        public async Task<ActionResult<DataResponseDto>> GetRevisionList([FromBody] RevisionFilterDto filters)
        {
            //init template repository
            EmailTemplateXmlFileRepository templateFileRepo = new EmailTemplateXmlFileRepository();

            Guid templateId = Guid.Parse(filters.Id);

            //get/set queryable templates => filtered by Active & IsDefault
            var emailTemplate = templateFileRepo.Single(templateId).Result;

            IQueryable<EmailTemplate>? revisionTemplates = null;
            int revisionsCount = 0;

            if (emailTemplate != null)
            {
                revisionsCount = emailTemplate.Versions.Length;
                revisionTemplates = emailTemplate.Versions.AsQueryable();
            }

            //return data response
            var dataResponse = new DataResponseDto
            {
                TotalTemplateCount = revisionsCount,
                TotalPages = 0,
                EmailTemplates = revisionTemplates
            };

            return Ok(dataResponse);
        }
    }
}