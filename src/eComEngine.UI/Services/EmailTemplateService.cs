﻿using eComEngine.Application.Interfaces;
using eComEngine.Application.Models.Dto;

namespace eComEngine.UI.Services
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly HttpClient _httpClient;

        public EmailTemplateService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<DataResponseDto> GetTemplatesAsync(string targetUri, DataFilterDto filters)
        {
            var response = await _httpClient.PostAsJsonAsync(targetUri, filters);
            return await response.Content.ReadFromJsonAsync<DataResponseDto>();
        }

        public async Task<DataResponseDto> GetRevisionsAsync(string targetUri, RevisionFilterDto filters)
        {
            var response = await _httpClient.PostAsJsonAsync(targetUri, filters);
            return await response.Content.ReadFromJsonAsync<DataResponseDto>();
        }
    }
}