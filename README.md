# This is my first Blazor server-side solution #

This is a simple attempt to solution a coding challenge from eComEngine.

### Technology Used ###

* ASP.NET Core 6
* Blazor Server Side

### How do I get set up? ###

* Just clone this repository
* Open the solution file with your current VS IDE
* Set to 'Multiple Start'
* The Start